package co.com.leantech.proxy;

public class Proxy {
	
	private static int DEFAULT_PORT = 8081;

	public static void main(String[] args) {
		
		int port = DEFAULT_PORT;
		try {
			port = Integer.parseInt(args[0]);
		}catch (Exception e) {
		}
		
        Server server = new Server(port);
        server.run();
    }
	
}
