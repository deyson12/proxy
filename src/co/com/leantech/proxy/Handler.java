package co.com.leantech.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Handler extends Thread {
	private final Socket clientSocket;
	private boolean previousWasR = false;

	public Handler(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {
		try {
			String request = readLine(clientSocket);
			System.out.println("\nREQUEST: " + request);
			String header = null;
			System.out.println("Headers:");

			boolean headerNotEmpty = true;
			while (headerNotEmpty) {
				header = readLine(clientSocket);
				
				if("".equals(header)) {
					headerNotEmpty = false;
				}else {
					System.out.println(header);	
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				System.err.println("Error closing the client Socket");
			}
		}
	}

	private String readLine(Socket socket) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		int next;

		readerLoop: while ((next = socket.getInputStream().read()) != -1) {
			if (previousWasR && next == '\n') {
				previousWasR = false;
				continue;
			}
			previousWasR = false;
			switch (next) {
			case '\r':
				previousWasR = true;
				break readerLoop;
			case '\n':
				break readerLoop;
			default:
				byteArrayOutputStream.write(next);
				break;
			}
		}
		return byteArrayOutputStream.toString("ISO-8859-1");
	}
}