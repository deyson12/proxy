package co.com.leantech.proxy;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

	private int port = 0;
	
    public Server(int port) {
        super();
        this.port = port;
    }

    @Override
    public void run() {
        try {
        	ServerSocket serverSocket = new ServerSocket(port);
        	
        	System.out.println("Proxy running on port " + port);
        	
            Socket socket;
            try {
                while ((socket = serverSocket.accept()) != null) {
                    new Handler(socket).start();
                }
            } catch (IOException e) {
            	System.err.println("Error reading the socket");
            }
        } catch (Exception e) {
           System.err.println(e.getMessage());
           System.exit(0);
        }
    }
}
