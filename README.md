<div  align="center">

<a  href="http://deysonestrada.com"><img  width="100%"  src="https://raw.githubusercontent.com/deyson12/deyson12/master/github.png"  alt="cover"  /></a>

</div>
## Hola, Soy Deyson Estrada and ❤️ code
<div>

<a  href="https://www.linkedin.com/in/dfep/"  target="_blank">

<img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white&color=071A2C" alt="LinkedIn">
</a>
<a  href="https://www.instagram.com/deysonestrad/"  target="_blank">

<img src="https://img.shields.io/badge/Instagram-%23E4405F.svg?&style=flat-square&logo=instagram&logoColor=white&color=071A2C" alt="Instagram">

</a>

<a  href="mailto:deyson12@gmail.com"  mailto="deyson12@gmail.com"  target="_blank">

<img src="https://img.shields.io/badge/Gmail-%231877F2.svg?&style=flat-square&logo=gmail&logoColor=white&color=071A2C" alt="Gmail">

</a>

<a  href="http://deysonestrada.com"  target="_blank">

<img src="https://img.shields.io/badge/Freelancer-%231877F2.svg?&style=flat-square&logo=freelancer&logoColor=white&color=071A2C" alt="Freelancer">

</a>

</div>

<br  />
<h2>Steps to exetute the Proxy Server</h2>

1. Compile and generate the .jar file

2. Go to the directory and ejexute, where 8080 is the port for default is 8081
```
java -jar proxy.jar 8080
```

3. You will see something like this
```
Proxy running on port 8080
```

4. Configure the proxy in the System. (Host: 127.0.0.1, Port: 8081)

5. Ready!!! Open a Page and the log of headers is printed on the console, like this.
```
Proxy running on port 8080

REQUEST: CONNECT www.youtube.com:443 HTTP/1.1
Headers:
Host: www.youtube.com:443
Proxy-Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML,
like Gecko) Chrome/96.0.4664.110 Safari/537.36

REQUEST: CONNECT www.gstatic.com:443 HTTP/1.1
Headers:
Host: www.gstatic.com:443
Proxy-Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML,
like Gecko) Chrome/96.0.4664.110 Safari/537.36

REQUEST: CONNECT fonts.gstatic.com:443 HTTP/1.1
Headers:
Host: fonts.gstatic.com:443
Proxy-Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML,
like Gecko) Chrome/96.0.4664.110 Safari/537.36
```
